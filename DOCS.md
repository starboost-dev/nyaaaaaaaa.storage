# how thingy work????

connect to ws://whatever:6630 (wss coming soon)  
send commands uwu
if you get a `error` or `info` returned, its likely human readable. you should show it to the user

[required parameter], (optional parameter)

## ACCOUNT MANAGEMENT

login  
: Used to log the user in.

`login\t[username]\t[password]`  
on success: `signedin\t[username]`  
on error: `error\t` the password or username is probably incorrect, blame user

register  
: Used to create an account.

`register\t[username]\t[password]`  
on success: `signedin\t[username]`  
on error: `error\t` username taken, or user doesnt know how to write a good username/pw

logout
: Used to log out.

`logout`  
on success: (the connection is closed)  

nickname
: Used to change your nickname (because usernames are limited). If no nickname supplied, sets nickname to usual username.

`nickname\t(nickname)`  
on success: returns info w/ new nickname

<small>soon: stuff like. profile picture. and more</small>

## CHAT

chat  
: Used to send a message to global chat.

`chat\t[message]`  
on success: `chat\tYou\t[message]\t[username]`  

tell  
: Used to send a DM to someone.

`tell\t[message]\t[destination]`  
on success: `tell\tYou -> [destination]\t[message]\t[username]`  
on error: User not found/offline error

the server will send you chat messages occasionally, in format of:
`chat\t[nickname]\t[message]\t[username]`
`tell\t[nickname] -> You\t[message]\t[username]`

## SOON

literally everything else. like. friends. leaderboards. downloading songs. uploading songs. searching for songs. etc.
