'''registration, logging into the game'''

import re

import bcrypt


def register(username, password, database, conn):
    '''registers user into database'''
    if not re.fullmatch(r'[a-zA-Z0-9_-]{3,30}', username):
        return "error\tUsername invalid (a-Z, 0-9, _ and - valid. 3-30 chars)"
    pwregex = r'(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}'
    if not re.fullmatch(pwregex, password):
        return """error\tPassword invalid
               (min 8chars, 1 uppercase, 1 lowercase, 1 number, 1 special)"""
    password = password.encode('utf-8')
    salt = bcrypt.gensalt()
    hashpw = bcrypt.hashpw(password, salt)
    if "users" not in database:
        database["users"] = {}
    if username in database["users"]:
        return "error\tUsername taken"
    database["users"][username] = {}
    database["users"][username]["password_hash"] = hashpw.decode()
    conn["logged_in"] = True
    conn["username"] = username
    conn["nickname"] = username
    return f"signedin\t{username}"


def login(username, password, database, conn):
    '''checks username/pw'''
    password = password.encode('utf-8')
    hashpw = database["users"][username]["password_hash"].encode('utf-8')
    authed = bcrypt.checkpw(password, hashpw)
    if not authed:
        return "error\tWrong password, or user not registered"
    conn["logged_in"] = True
    conn["username"] = username
    conn["nickname"] = database["users"][username].get("nickname", username)
    return f"signedin\t{username}"


def nickset(username, database, conn, nickname=None):
    '''sets nickname (to nothing if empty)'''
    if nickname is None:
        nickname = username
    database["users"][username]["nickname"] = nickname
    conn["nickname"] = nickname
    return f"info\tNickname set to {nickname}"
