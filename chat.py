'''chat handler'''


def filter_message(message):
    '''function that filters message for bad stuff'''
    #  TODO: idk filter slurs or something
    return message


async def global_send(message, send_fn, conns, cndata, user_id):
    '''sends message globally'''
    text = filter_message(message)
    msg = "chat"
    msg += "\t" + cndata[user_id]['nickname']
    msg += "\t" + text
    msg += "\t" + cndata[user_id]['username']
    for usr in conns:
        if usr != user_id:
            await send_fn(msg, usr)
    return f"chat\tYou\t{text}\t{cndata[user_id]['username']}"


async def dm_send(message, destination, send_fn, cndata, user_id):
    '''sends dm'''
    text = filter_message(message)
    msg = "tell"
    msg += "\t" + cndata[user_id]['nickname'] + " -> You"
    msg += "\t" + text
    msg += "\t" + cndata[user_id]['username']
    success = False
    for usr in cndata:
        if (destination == cndata[usr]["username"]
           and usr != user_id):
            success = True
            await send_fn(msg, usr)
    if not success:
        return f"error\tUser {destination} not found or offline"
    return f"tell\tYou -> {destination}\t{text}\t{cndata[user_id]['username']}"
