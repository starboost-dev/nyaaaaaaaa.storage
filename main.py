'''server'''

import asyncio
import uuid
import json
import signal
import shutil
import time
import sys

import websockets

import account
import chat
import tools

CONNECTIONS = {}
CONN_DATA = {}
db = {}
shutil.copyfile("db.json", f".{time.time()}.db.json")
with open("db.json", "r", encoding="utf8") as dbj:
    db = json.load(dbj)


# pylint: disable=too-many-branches
async def action(message, user_id):
    '''process messages from game'''
    message = message.split("\t")  # we split stuff by tabs for simplicity
    message = [cmd.strip() for cmd in message]
    if not CONN_DATA[user_id]["logged_in"]:
        if message[0] != "login" and message[0] != "register":
            await send("error\tLogin first", user_id)
            return
    res = "error\tUnknown command"
    if message[0] == "login":
        if not tools.check_arr(message, 1):
            res = "error\tYou need to specify a username"
        elif not tools.check_arr(message, 2):
            res = "error\tYou need to specify a password"
        else:
            res = account.login(message[1], message[2],
                                db, CONN_DATA[user_id])
            print(f"login: {message[1]}, {user_id}")
    if message[0] == "register":
        if not tools.check_arr(message, 1):
            res = "error\tYou need to specify a username"
        elif not tools.check_arr(message, 2):
            res = "error\tYou need to specify a password"
        else:
            res = account.register(message[1], message[2],
                                   db, CONN_DATA[user_id])
            print(f"register: {message[1]}, {user_id}")
    if message[0] == "nickname":
        res = account.nickset(CONN_DATA[user_id]['username'],
                              db,
                              CONN_DATA[user_id],
                              message[1] if 1 < len(message) else None)
    if message[0] == "logout":
        print(f"logout: {CONN_DATA[user_id]['username']}, {user_id}")
        await close(user_id)
        return
    if message[0] == "chat":
        if tools.check_arr(message, 1):
            res = await chat.global_send(message[1],
                                         send,
                                         CONNECTIONS,
                                         CONN_DATA,
                                         user_id)
    if message[0] == "tell":
        if not tools.check_arr(message, 1):
            res = "error\tYou need to specify a message"
        if not tools.check_arr(message, 2):
            res = "error\tYou need to specify a recipent"
        else:
            res = await chat.dm_send(message[1],
                                     message[2],
                                     send,
                                     CONN_DATA,
                                     user_id)
    await send(res, user_id)


async def handle(websocket):
    '''takes in connections, loops them 5ever, sends them to action'''
    user_id = uuid.uuid4().hex
    CONNECTIONS[user_id] = websocket
    CONN_DATA[user_id] = {"logged_in": False}
    try:
        async for message in websocket:
            await action(message, user_id)
    except websockets.exceptions.ConnectionClosedError:
        print(f"disconnect: {user_id}")
    finally:
        del CONNECTIONS[user_id]
        del CONN_DATA[user_id]


async def close(user_id, msg="closing connection..."):
    '''close a connection'''
    websocket = CONNECTIONS[user_id]
    await websocket.close(1000, msg)


async def send(msg, user_id):
    '''send message back in the websocket'''
    websocket = CONNECTIONS[user_id]  # raises KeyError if user disconnected
    await websocket.send(msg)  # may raise websockets.ConnectionClosed


async def main(ip_bind="", port=6630):
    '''runs everything 5ever'''
    async with websockets.serve(handle, ip_bind, port):
        print(f"running on {'localhost' if ip_bind == '' else ip_bind}:{port}")
        await asyncio.Future()  # run forever


def ctrlc(_='', __=''):
    '''quit server safely'''
    print("saving database before quitting...")
    with open("db.json", "w", encoding="utf8") as dbstr:
        json.dump(db, dbstr)
    sys.exit()


signal.signal(signal.SIGINT, ctrlc)
asyncio.run(main())
